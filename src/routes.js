const express = require("express");
const mongoose = require("mongoose");
const passport = require("passport");

const router = express.Router();
const productModel = mongoose.model("products");
const userModel = mongoose.model("user");

router.route("/login").post((req, res, next) => {
  if (req.body.username && req.body.password) {
    passport.authenticate("local", function (error, user) {
      if (error) return res.status(500).send(error);
      req.logIn(user, function (error) {
        if (error) return res.status(500).send(error);
        return res.status(200).send("Sikeres bejelentkezés");
      });
    })(req, res);
  } else {
    return res
      .status(400)
      .send("Hibás kérés, add meg a felhasználónevet és a jelszót is!");
  }
});

router.route("/logout").post((req, res, next) => {
  if (req.isAuthenticated()) {
    req.logOut();
    return res.status(200).send("Sikeres kijelentkezés!");
  } else {
    return res.status(403).send("Nincs bejelentkezett felhasználó!");
  }
});

router.route("/status").post((req, res, next) => {
  if (req.isAuthenticated()) {
    return res.status(200).send(req.session);
  } else {
    return res.status(403).send("Nincs bejelentkezett felhasználó!");
  }
});

router
  .route("/user")
  .get((req, res, next) => {
    userModel.find({}, (err, user) => {
      if (err) return res.status(500).send("DB hiba");
      res.status(200).send(user);
    });
  })
  .post((req, res, next) => {
    console.log(req.body);
    if (req.body.username && req.body.email && req.body.password) {
      userModel.findOne({ username: req.body.username }, (err, user) => {
        if (user) {
          return res.status(400).send("Már létezik ilyen felhasználó");
        }
      });
      const user = new userModel({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        fullName: req.body.fullName,
        phone: req.body.phone,
        address: req.body.address,
      });
      user.save((error) => {
        if (error) return res.status(500).send(error);
        return res.status(200).send("Sikeres felhasználó mentés");
      });
    } else {
      return res.status(400).send("Hibás username vagy password");
    }
  });

router.route("/toBasket").put((req, res, next) => {
  console.log(req.body);
  if (req.body.username) {
    userModel.findOne({ username: req.body.username }, (err, user) => {
      if (err) return res.status(500).send("DB hiba");
      if (user) {
        if (!user.basket) {
          user.basket = [req.body.productId];
          console.log(user);
        } else {
          user.basket = [...user.basket, req.body.productId];
        }
        user.save((error) => {
          if (error) return res.status(500).send(error);
          return res.status(200).send("Sikeres hozzáadás");
        });
      } else {
        return res.status(400).send("Nincs ilyen azonosítójú user!");
      }
    });
  } else {
    return res.status(400).send("Sikertelen");
  }
});

// router.route("/getBasket").get((req, res, next) => {
//   const products = [];
//   userModel.findOne({ username: req.body.username }, (err, user) => {
//     if (err) return res.status(500).send("DB hiba");
//     if (user) {
//       if (user.basket.length > 0) {
//         console.log(user.basket);
//         user.basket.map((productId) => {
//           let productidifi = productId.toString();
//           console.log(productidifi);
//           productModel.findById(productId.toString(), (error, product) => {
//             if (error) return res.status(500).send(error);
//             products.push(product);
//             console.log("belül", products);
//           });
//           console.log("kivus", products);
//         });
//       }
//     }
//     return res.status(200).send(products);
//   });
// });

router
  .route("/product")
  .get((req, res, next) => {
    productModel.find({}, (err, products) => {
      if (err) return res.status(500).send("DB hiba");
      res.status(200).send(products);
    });
  })
  .post((req, res, next) => {
    console.log(req.body);
    if (req.body.id) {
      productModel.findOne({ id: req.body.id }, (err, product) => {
        if (err) return res.status(500).send("DB hiba");
        if (product) {
          return res.status(400).send("Már van ilyen id");
        } else {
          const product = new productModel({
            id: req.body.id,
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            quantity: req.body.quantity,
          });
          product.save((error) => {
            if (error)
              return res.status(500).send("Hiba történt a mentés során.");
            return res.status(200).send("Sikeres mentés");
          });
        }
      });
    } else {
      return res.status(400).send("Nincs megadva azonosító!");
    }
  })
  .put((req, res, next) => {
    console.log(req.body);
    if (req.body.id) {
      productModel.findOne({ id: req.body.id }, (err, product) => {
        if (err) return res.status(500).send("DB hiba");
        if (product) {
          product.name = req.body.name;
          product.description = req.body.description;
          product.price = req.body.price;
          product.quantity = req.body.quantity;
          product.save((error) => {
            if (error)
              return res.status(500).send("Hiba történt a mentés során.");
            return res.status(200).send("Sikeres mentés");
          });
        } else {
          return res.status(400).send("Nincs ilyen azonosítójú termék!");
        }
      });
    } else {
      return res.status(400).send("Nincs megadva azonosító!");
    }
  })
  .delete((req, res, next) => {
    console.log(req.body);
    if (req.body.id) {
      productModel.findOne({ id: req.body.id }, (err, product) => {
        if (err) return res.status(500).send("DB hiba");
        if (product) {
          product.delete((error) => {
            if (error)
              return res.status(500).send("Hiba történt a törlés során.");
            return res.status(200).send("Sikeres törlés");
          });
        } else {
          return res.status(400).send("Nincs ilyen azonosítójú termék!");
        }
      });
    } else {
      return res.status(400).send("Nincs megadva azonosító!");
    }
  });

module.exports = router;
