const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const passport = require("passport");
const expressSession = require("express-session");
const localStrategy = require("passport-local");
const path = require("path");

const app = express();
const port = process.env.PORT || 3000;
const dbUrl =
  "mongodb+srv://admin:admin123@prf-cluster.jedkt.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";

mongoose.connect(dbUrl);
mongoose.connection.on("connected", () => {
  console.log("sikeres db csatlakozas");
});

mongoose.connection.on("error", (err) => {
  console.log("Hiba tortent: ", err);
});

require("./db/product.model");
require("./db/user.model");

const userModel = mongoose.model("user");

app.use(cookieParser());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(express.json());

const whiteList = [
  "http://localhost:4200",
  "https://prf-kotprog-2022.herokuapp.com",
];

// app.use(
//   cors({
//     origin: function (origin, callback) {
//       if (whiteList.indexOf(origin) >= 0) {
//         callback(null, true);
//       } else {
//         callback(new Error("CORS ERROR"));
//       }
//     },
//     credentials: true,
//     methods: "GET,PUT,POST,DELETE,OPTIONS",
//   })
// );

app.use(cors());

// app.use((req, res, next) => {
//   res.set("Access-Control-Allow-Origin", "http://localhost:4200");
//   res.set("Access-Control-Allow-Credentials", "true");
//   if (req.method === "OPTIONS") {
//     res.set(
//       "Access-Control-Allow-Methods",
//       "GET,PUT,POST,DELETE,PATCH,OPTIONS"
//     );
//     res.set("Access-Control-Allow-Headers", "Content-Type");
//     res.set("Access-Control-Max-Age", "3600");
//   }
//   next();
// });

passport.use(
  "local",
  new localStrategy.Strategy(function (username, password, done) {
    userModel.findOne({ username: username }, function (err, user) {
      if (err) return done("Hiba a lekérés során", null);
      if (!user) return done("Nincs ilyen felhasználónév");
      user.comparePasswords(password, function (error, isMatch) {
        if (error) return done(error, false);
        if (!isMatch) return done("Hibás jelszó", false);
        return done(null, user);
      });
    });
  })
);

passport.serializeUser(function (user, done) {
  if (!user) return done("Nincs megadva beléptethető felhasználó!", null);
  return done(null, user);
});

passport.deserializeUser(function (user, done) {
  if (!user) return done("Nincs felhasználó akit kiléptethetnénk!", null);
  return done(null, user);
});

app.use(expressSession({ secret: "Sg8bw0AaoV5kln9pQigL", resave: true }));
app.use(passport.initialize());
app.use(passport.session());

// app.get("/", (req, res, next) => {
//   res.send("Hello World!");
// });

app
  .use(express.static(path.join(__dirname, "public")))
  .set("views", path.join(__dirname, "views"))
  .set("vies engine", "ejs")
  .get("/", (req, res) => res.render("/pages/index"));

app.use("/", require("./routes"));

app.use((req, res, next) => {
  console.log("404");
  res.status(404).send("Az oldal nem található!");
});

app.listen(port, () => {
  console.log("a szerver fut");
});
