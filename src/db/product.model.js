const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
  {
    id: { type: Number, unique: true, required: true },
    name: { type: String, required: true },
    description: { type: String, required: true },
    price: { type: Number, required: true },
    quantity: { type: Number, required: true },
  },
  { collection: "products" }
);

mongoose.model("products", productSchema);
