const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema(
  {
    username: { type: String, unique: true, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    fullName: { type: String, required: true },
    phone: { type: String },
    address: { type: String },
    accessLevel: { type: String },
    basket: { type: [mongoose.Types.ObjectId] },
  },
  { collection: "users" }
);

userSchema.pre("save", function (next) {
  const user = this;
  if (user.isModified("password")) {
    user.accessLevel = "user";
    bcrypt.genSalt(10, function (error, salt) {
      if (error) {
        console.log("hiba a salt generálása során");
        return next(error);
      }
      bcrypt.hash(user.password, salt, function (error, hash) {
        if (error) {
          console.log("hiba a salt hashelés során");
          return next(error);
        }
        user.password = hash;
        return next();
      });
    });
  } else {
    return next();
  }
});

userSchema.methods.comparePasswords = function (password, nx) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    nx(err, isMatch);
  });
};

mongoose.model("user", userSchema);
