# PRF-2022

## Mongo DB

Collections:

### User:

- username
- email
- fullName
- phone
- address
- basket: [{productId, quantity}]
- orders: [{products: [{productId, quantity}], orderDate}]

### Product:

- id
- name
- description
- price
- quantity

CRUD:

User:

- registration
- login
- log out
- get basket
- get orders
- save order

Product:

- get all
- get by id
- get by search
- add new product
- delete product
- edit product (quantity)
